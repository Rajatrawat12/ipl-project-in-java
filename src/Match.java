public class Match {
private String id;
private String season;
private String date;
private String team1;
    private String team2;
    private String toss_winner;
    private String toss_decision;
    private String result;
    private String dl_applied;
    private String winner;
    private String win_by_runs;
    private String win_by_wickets;
    private String player_of_match;
    private String venue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public String getToss_winner() {
        return toss_winner;
    }

    public void setToss_winner(String toss_winner) {
        this.toss_winner = toss_winner;
    }

    public String getToss_decision() {
        return toss_decision;
    }

    public void setToss_decision(String toss_decision) {
        this.toss_decision = toss_decision;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDl_applied() {
        return dl_applied;
    }

    public void setDl_applied(String dl_applied) {
        this.dl_applied = dl_applied;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getWin_by_runs() {
        return win_by_runs;
    }

    public void setWin_by_runs(String win_by_runs) {
        this.win_by_runs = win_by_runs;
    }

    public String getWin_by_wickets() {
        return win_by_wickets;
    }

    public void setWin_by_wickets(String win_by_wickets) {
        this.win_by_wickets = win_by_wickets;
    }

    public String getPlayer_of_match() {
        return player_of_match;
    }

    public void setPlayer_of_match(String player_of_match) {
        this.player_of_match = player_of_match;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }
}
