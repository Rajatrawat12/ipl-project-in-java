import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

public class Main {

    public static final int ID = 0;
    public static final int SEASON = 1;
    public static final int WINNER = 10;

    public static final int MATCHID = 0;
    public static final int EXTRARUNS = 16;
    public static final int BOWLINGTEAM = 3;
    public static final int BOWLER = 8;
    public static final int TOTALRUNS = 17;
    public static final int TOSSWINNER = 6;

    public static void main(String[] args) {
        List<Match> matches = getMatchDataBase();
        List<Delivery> deliveries = getDeliveryDataBase();


        findNumberMatchPlayedPerYear(matches);
        findNumberOfMatchesWinByTeams(matches);
        findExtraRunsConcededIn2016(matches, deliveries);
        findTopEconomicalBowler2015(matches, deliveries);
        findHeighestTimeMatchWinsAndTossWinsByATeam(matches);
        findmatcheswonByRoyalChallengersBangaloreIn2008(matches);
    }

    private static void findmatcheswonByRoyalChallengersBangaloreIn2008(List<Match> matches) {
        Map<String,Integer>result=new HashMap<>();
        int value=0;
        for(Match team : matches){
            if(team.getSeason().equals("2008")){
                if(team.getWinner().equals("Royal Challengers Bangalore")){
                    value++;
                    result.put(team.getWinner(),value);
                }

            }
        }
        System.out.println(result);
    }
    private static void findHeighestTimeMatchWinsAndTossWinsByATeam(List<Match> matches) {
        Map<String, Integer> result = new HashMap<>();
        for (Match match : matches) {
            if (match.getToss_winner().equals(match.getWinner())) {
                if (result.containsKey(match.getWinner())) {
                    int value = result.get(match.getWinner());
                    value++;
                    result.put(match.getWinner(), value);
                } else {
                    result.put(match.getWinner(), 1);
                }

            }
        }
        int highestWinTossAndMatch = Collections.max(result.values());
        for (String name : result.keySet()) {
            if (result.get(name) == highestWinTossAndMatch) {
                System.out.println("The Heights number of time match and toss win");
                System.out.println(name + "=" + highestWinTossAndMatch);
            }
        }
    }

    private static void findTopEconomicalBowler2015(List<Match> matches, List<Delivery> deliveries) {
        List<String> array = new ArrayList<>();
        for (Match match : matches) {
            if (match.getSeason().equals("2015")) {
                array.add(match.getId());
            }
        }
        Map<String, Integer> runs = new HashMap<>();
        Map<String, Integer> balls = new HashMap<>();
        Map<String, Double> overs = new HashMap<>();
        Map<String, Double> economy = new HashMap<>();
        for (Delivery delivery : deliveries) {
            if (array.contains(delivery.getMatch_id())) {
                if (balls.containsKey(delivery.getBowler())) {
                    int value = balls.get(delivery.getBowler());
                    value++;
                    balls.put(delivery.getBowler(), value);
                    int overValue = (int) balls.get(delivery.getBowler());
                    overs.put(delivery.getBowler(), (double) overValue / 6);
                } else {
                    balls.put(delivery.getBowler(), 1);
                }
                if (runs.containsKey(delivery.getBowler())) {
                    int value = runs.get(delivery.getBowler());
                    int number = Integer.parseInt(delivery.getTotal_runs());
                    value = value + number;
                    runs.put(delivery.getBowler(), value);

                } else {
                    runs.put(delivery.getBowler(), Integer.parseInt(delivery.getTotal_runs()));
                }
                if (economy.containsKey(delivery.getBowler())) {
                    int run = runs.get(delivery.getBowler());
                    double over = overs.get(delivery.getBowler());
                    double ecoValue = run / over;
                    economy.put(delivery.getBowler(), ecoValue);
                } else {
                    economy.put(delivery.getBowler(), 1.00);
                }
            }
        }
        double minEconomy = Collections.min(economy.values());
        for (String name : economy.keySet()) {
            if (economy.get(name) == minEconomy) {
                System.out.println("Top Economical Bowler Of 2015");
                System.out.println(name + "=" + minEconomy);

            }
        }

    }

    private static List<Match> getMatchDataBase() {
        List<Match> matches = new ArrayList<>();
        String file = "/home/rajat/IdeaProjects/IPLPOSO/src/matches.csv";
        BufferedReader reader = null;
        String line;
        try {
            reader = new BufferedReader(new FileReader(file));
            while ((line = reader.readLine()) != null) {
                String[] row = line.split(",");
                Match match = new Match();
                match.setId(row[ID]);
                match.setSeason(row[SEASON]);
                match.setWinner(row[WINNER]);
                match.setToss_winner(row[TOSSWINNER]);

                matches.add(match);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return matches;
    }

    private static List<Delivery> getDeliveryDataBase() {
        String file1 = "/home/rajat/IdeaProjects/IPLPOSO/src/deliveries.csv";
        String line1;
        BufferedReader reader1 = null;
        List<Delivery> deliveries = new ArrayList<>();
        try {
            reader1 = new BufferedReader(new FileReader(file1));
            while ((line1 = reader1.readLine()) != null) {
                String[] row1 = line1.split(",");
                Delivery delivery = new Delivery();
                delivery.setMatch_id(row1[MATCHID]);
                delivery.setExtra_runs(row1[EXTRARUNS]);
                delivery.setBowling_team(row1[BOWLINGTEAM]);
                delivery.setBowler(row1[BOWLER]);
                delivery.setTotal_runs(row1[TOTALRUNS]);
                deliveries.add(delivery);

            }

        } catch (Exception e) {
            System.out.println("File not found");
        }
        return deliveries;
    }

    private static void findExtraRunsConcededIn2016(List<Match> matches, List<Delivery> deliveries) {
        List<String> arr = new ArrayList<>();
        for (Match match : matches) {
            if (match.getSeason().equals("2016")) {
                arr.add(match.getId());
            }
        }
        Map<String, Integer> result = new HashMap<>();
        for (Delivery delivery : deliveries) {
            if (arr.contains(delivery.getMatch_id())) {
                if (result.containsKey(delivery.getBowling_team())) {
                    int value = result.get(delivery.getBowling_team());
                    int number = Integer.parseInt(delivery.getExtra_runs());
                    value = value + number;
                    result.put(delivery.getBowling_team(), value);
                } else {
                    result.put(delivery.getBowling_team(), Integer.parseInt(delivery.getExtra_runs()));
                }
            }
        }
        System.out.println(result);
    }


    private static void findNumberOfMatchesWinByTeams(List<Match> matches) {
        Map<String, Integer> number = new HashMap<>();
        for (Match match : matches) {
            if (match.getWinner() != "") {
                if (number.containsKey(match.getWinner())) {
                    int value = number.get(match.getWinner());
                    value++;
                    number.put(match.getWinner(), value);
                } else {
                    number.put(match.getWinner(), 1);
                }

            }
        }
        System.out.println(number);
    }

    private static void findNumberMatchPlayedPerYear(List<Match> matches) {
        Map<String, Integer> season = new HashMap<>();
        for (Match match : matches) {
            if (season.containsKey(match.getSeason())) {
                int value = season.get(match.getSeason());
                value++;
                season.put(match.getSeason(), value);

            } else {
                season.put(match.getSeason(), 1);
            }

        }
        System.out.println(season);
    }
}



